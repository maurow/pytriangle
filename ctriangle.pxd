# following http://docs.cython.org/src/tutorial/clibraries.html

cdef extern from "triangle.h":
    struct triangulateio:
        double *pointlist                                               # In / out 
        double *pointattributelist                                      # In / out 
        int *pointmarkerlist                                          # In / out 
        int numberofpoints                                            # In / out 
        int numberofpointattributes                                   # In / out 
        
        int *trianglelist                                             # In / out 
        double *triangleattributelist                                   # In / out 
        double *trianglearealist                                         # In only 
        int *neighborlist                                             # Out only 
        int numberoftriangles                                         # In / out 
        int numberofcorners                                           # In / out 
        int numberoftriangleattributes                                # In / out 
        
        int *segmentlist                                              # In / out 
        int *segmentmarkerlist                                        # In / out 
        int numberofsegments                                          # In / out 
        
        double *holelist                        # In / pointer to array copied out 
        int numberofholes                                      # In / copied out 

        double *regionlist                      # In / pointer to array copied out 
        int numberofregions                                    # In / copied out 
        
        int *edgelist                                                 # Out only 
        int *edgemarkerlist            # Not used with Voronoi diagram out only 
        double *normlist                # Used only with Voronoi diagram out only 
        int numberofedges                                             # Out only 

    void triangulate(char *,  triangulateio *,  triangulateio *,  triangulateio *)
    # void trifree(void *)
    # doens't compile: void triangulate(char *, struct triangulateio *,  struct triangulateio *, struct triangulateio *)

    # ctypedef struct triangulateio:
    #     pass
    # void triangulate(char *, triangulateio *, triangulateio *, triangulateio *)
