"""
To specify the geometry of the problem.
"""

class Geometry(object):
    pass

class GeometryPoint(Geometry):
    pass

class GeometryPSLG(Geometry):
    pass
