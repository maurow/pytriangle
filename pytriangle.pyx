#!/usr/bin/env python
"""
Provides a class which holds the triangular mesh

TODO:
- implement identical interface as matplotlib.tri.Triangulation
- do the deallocation method

Cython:
http://docs.cython.org/src/userguide
C-libaries: http://docs.cython.org/src/tutorial/clibraries.html
Numpy: 
Other useful info:
http://stackoverflow.com/questions/3046305/simple-wrapping-of-c-code-with-cython
strings: http://wiki.cython.org/enhancements/stringliterals
"""

from __future__ import division
cimport cython
from libc.stdlib cimport free
import numpy as np
cimport numpy as np
np.import_array() #http://stackoverflow.com/questions/8366095/have-pointer-to-data-need-numpy-array-in-fortran-order-want-to-use-cython
import pylab as plt
# my stuff
#cimport ctriangle

# my stuff
from ctriangle cimport triangulateio, triangulate


cdef class Triio:
    # not possible to specifiy the dtype of the array here
    cdef public np.ndarray points_in, pointattrs_in, pointmarkers_in
    cdef public np.ndarray points, edges, connect
    # note: pointers to memoryviews are not supported thus using np.array

    cdef public object switches
    
    cdef triangulateio tri_in, tri_out, tri_vor

    # __cinit__ needs to initialise all the c-variables. __init__ is called afterwards
    def __cinit__(self, np.ndarray[np.double_t,ndim=2,mode="c"] points_in, switches=b"-V -q25 -a0.1",
                  np.ndarray[np.double_t,ndim=2,mode="c"] pointattrs_in=np.array([[],[]],dtype=np.double),
                  np.ndarray[np.int_t,ndim=2,mode="c"] pointmarkers_in=np.array([[],[]],dtype=np.int)):
        """
        Switches -Devz are implied
        
        References
        - http://codespeak.net/pipermail/cython-dev/2008-October/002669.html
        """
        # some stuff with the new unicode strings.  byte strings seem the normal now
        if type(switches)!=bytes:
            self.switches = switches.encode('UTF-8')
        else:
            self.switches = switches
        self.switches += b' -Devz'
        
        # note that haveing these ndarrays serves both as interface to python-space
        # as well as keeping a python-reference to the memory to avoid garbabge collection
        self.points_in = points_in
        self.pointattrs_in = pointattrs_in
        self.pointmarkers_in = pointmarkers_in

        # .shape needs to be accessed with index, otherwise error
        npts = points_in.shape[0]
        self.tri_in.numberofpoints = npts
        self.tri_in.numberofpointattributes = pointattrs_in.shape[0]
        self.tri_in.pointlist = <double *> points_in.data
        
        if pointattrs_in.shape[0]==0:
            self.tri_in.pointattributelist = NULL
        else:
            self.tri_in.pointattributelist = <double *> pointattrs_in.data

        if pointmarkers_in.shape[0]==0:
            self.tri_in.pointmarkerlist = NULL
        else:
            self.tri_in.pointmarkerlist = <int *> pointmarkers_in.data

        # rest, for now, intialize to empty:
 
        # triangles (set when using r switch)
        self.tri_in.trianglelist = NULL
        self.tri_in.triangleattributelist = NULL
        self.tri_in.trianglearealist = NULL
        self.tri_in.neighborlist = NULL
        self.tri_in.numberoftriangles = 0  
        self.tri_in.numberofcorners = 0
        self.tri_in.numberoftriangleattributes = 0

        # with using lines (nsn) (p swithch)
        self.tri_in.segmentlist = NULL
        self.tri_in.segmentmarkerlist = NULL
        self.tri_in.numberofsegments = 0

        # nsn
        self.tri_in.holelist = NULL
        self.tri_in.numberofholes = 0

        self.tri_in.regionlist = NULL
        self.tri_in.numberofregions = 0

        # output
        self.tri_in.edgelist = NULL
        self.tri_in.edgemarkerlist = NULL
        self.tri_in.normlist = NULL
        self.tri_in.numberofedges = 0


    def ret_pts(self):
        for ii in range(self.tri_in.numberofpoints*2):
            print(self.tri_in.pointlist[ii])
    def plot_mesh(self):
        """
        Plots the mesh using pylab.triplot.
        """
        plt.triplot(self.points[:,0], self.points[:,1], self.connect)
        plt.show()

    # def __repr__(self):
    #     return 'Trio instance\n' + str(self.tri_in.numberofpoints) + ' ' + str(self.tri_in.numberofpoints)

    def triangulate(self):
        """
        Does the triangulation.
        """
        triangulate(<char *>self.switches, &self.tri_in, &self.tri_out, &self.tri_vor)
        self._populate_output()
    def refine(self):
        """
        Not yet implemented
        """
        pass
        
    cdef void _populate_output(self):
        # http://stackoverflow.com/questions/8366095/have-pointer-to-data-need-numpy-array-in-fortran-order-want-to-use-cython
        #  http://docs.scipy.org/doc/numpy/user/c-info.how-to-extend.html

        cdef np.npy_intp *lenp = [self.tri_out.numberofpoints, 2]
        self.points = np.PyArray_SimpleNewFromData(2,
                                                   lenp,
                                                   np.NPY_DOUBLE,
                                                   self.tri_out.pointlist)

        cdef np.npy_intp *lene = [self.tri_out.numberofedges, 2]        
        self.edges = np.PyArray_SimpleNewFromData(2,
                                                  lene,
                                                  np.NPY_INT,
                                                  self.tri_out.edgelist)

        cdef np.npy_intp *lenc = [self.tri_out.numberoftriangles, 3]                
        self.connect = np.PyArray_SimpleNewFromData(2,
                                                       lenc,
                                                       np.NPY_INT,
                                                       self.tri_out.trianglelist)
        
        
    cdef _free_triio(self, triangulateio tri_io):
        """
        Frees the memory of pointers in a triangulateio structure
        which was allocated by the call to Triangle.
        """
        free(tri_io.pointlist)
        free(tri_io.pointattributelist)
        free(tri_io.pointmarkerlist)
        free(tri_io.trianglelist)
        free(tri_io.triangleattributelist)
        free(tri_io.trianglearealist)
        free(tri_io.neighborlist)
        free(tri_io.segmentlist)
        free(tri_io.segmentmarkerlist)
        free(tri_io.holelist)
        free(tri_io.regionlist)
        free(tri_io.edgelist)
        free(tri_io.edgemarkerlist)
        free(tri_io.normlist)
        
    def __dealloc__(self):
        """
        Should free all stuff which was allocated by trinagle
        """
        self._free_triio(self.tri_out)
        self._free_triio(self.tri_vor)
