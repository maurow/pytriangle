import pytriangle as pyt
import numpy as np

def factory():
    points = np.array([0,0,0,1,1,1,1,0], dtype=np.double).reshape((4,2))
    #error: points = np.array([[0,0],[0,1],[1,1],[1,0]], dtype=np.double, order='F')
    #error: points = points[0::2,:]
    tmp =pyt.Triio(points, switches=b"-V -q25 -a0.001")
    tmp.ret_pts()
    return tmp

tri = factory()

tri.triangulate()
